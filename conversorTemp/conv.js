function converter() {
    let tempEntrada = Number(document.getElementById("idTemp").value)
    let unidadeEntrada = document.querySelector("#idUnidadeOrigem")
    let unidadeEntradaValor = unidadeEntrada.options[unidadeEntrada.selectedIndex].value;
    let unidadeConvesao = document.querySelector("#idUnidadeConvertido")
    let unidadeConvesaoValor = unidadeConvesao.options[unidadeConvesao.selectedIndex].value;

    //Conversao da unidade de entrada para Celsius 
    let tempCesius

    switch (unidadeEntradaValor) {
        case "C":
            tempCesius = tempEntrada
            break;

        case "F":
            tempCesius = (tempEntrada * 5 - 160) / 9
            break;

        case "K":
            tempCesius = tempEntrada - 273
            break;

        default:
            break;
    }

    //Conversao de celsius para a unidade de saida 
    let tempConvertido
    switch (unidadeConvesaoValor) {
        case "C":
            tempConvertido = tempCesius
            break;
        case "F":
            tempConvertido = (9 * tempCesius + 160) / 5
            break;
        case "K":
            tempConvertido = tempCesius + 273
            break;
        default:
            break;
    }

    document.getElementById("idTempConvertido").value = tempConvertido;

  
}
